package de.externalwebservices.worms.aphiav1_0;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.json.JsonObject;

import org.gfbio.wormswebservice.WORMSWSServiceIMPL;
import org.junit.BeforeClass;
import org.junit.Test;

public class WORMWSTest {
	
	static WORMSWSDAO wDAO;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		wDAO = new WORMSWSDAO();
		
	}
    @Test
    public void WSSynonymsTest() {
         System.out.println("*** Test synonym service ***");
         WORMSWSServiceIMPL serviceIMPL = new WORMSWSServiceIMPL();
         ArrayList<JsonObject> check = serviceIMPL.getSynonyms("http://www.marinespecies.org/aphia.php?p=taxdetails&id=1410").create();
         assertNotNull(check);
         System.out.println(check);
    }
   @Test
    public void WSMetadata() {
         System.out.println("*** Test metadata service ***");
         WORMSWSServiceIMPL serviceIMPL = new WORMSWSServiceIMPL();
         ArrayList<JsonObject> check = serviceIMPL.getMetadata().create();
         assertNotNull(check);
         System.out.println(check);
    }
    @Test
    public void WSSearchTest() {
         System.out.println("*** Test search service ***");
         WORMSWSServiceIMPL serviceIMPL = new WORMSWSServiceIMPL();
         ArrayList<JsonObject> check = serviceIMPL.search("klegg", "exact").create();
         assertNotNull(check);
         for(JsonObject a : check){
             System.out.println(a.toString());
         }
    }
   @Test
    public void WSHierarchy() {
         System.out.println("*** Test hierarchy service ***");
         WORMSWSServiceIMPL serviceIMPL = new WORMSWSServiceIMPL();
         ArrayList<JsonObject> check = serviceIMPL.getHierarchy("http://www.marinespecies.org/aphia.php?p=taxdetails&id=113447").create();
         assertNotNull(check);
         for(JsonObject a : check){
             System.out.println(a.toString());
         }
    }
   @Test
   public void WSAllbroader() {
        System.out.println("*** Test allbroader service ***");
        WORMSWSServiceIMPL serviceIMPL = new WORMSWSServiceIMPL();
        ArrayList<JsonObject> check = serviceIMPL.getAllBroader("http://www.marinespecies.org/aphia.php?p=taxdetails&id=113447").create();
        assertNotNull(check);
        for(JsonObject a : check){
            System.out.println(a.toString());
        }
   }
   @Test
   public void testTermserviceO() {
       System.out.println("*** Test term original service ***");
       WORMSWSServiceIMPL wormswsServiceIMPL = new WORMSWSServiceIMPL();
       ArrayList<JsonObject> a = wormswsServiceIMPL.getTermInfosOriginal("http://www.marinespecies.org/aphia.php?p=taxdetails&id=1410").create();
       for(JsonObject n: a){
           System.out.println(n);
       }
   }
  @Test
   public void testTermserviceP() {
      System.out.println("*** Test term processed service ***");
       WORMSWSServiceIMPL wormswsServiceIMPL = new WORMSWSServiceIMPL();
       ArrayList<JsonObject> a = wormswsServiceIMPL.getTermInfosProcessed("http://www.marinespecies.org/aphia.php?p=taxdetails&id=1410").create();
       for(JsonObject n: a){
           System.out.println(n);
       }
   }
   @Test
   public void testTermserviceC() {
       System.out.println("*** Test term combined service ***");
       WORMSWSServiceIMPL wormswsServiceIMPL = new WORMSWSServiceIMPL();
       ArrayList<JsonObject> a = wormswsServiceIMPL.getTermInfosCombined("http://www.marinespecies.org/aphia.php?p=taxdetails&id=1410").create();
       for(JsonObject n: a){
           System.out.println(n);
       }
   }
//	/**
//	 * this example is used here : http://www.marinespecies.org/aphia.php?p=taxdetails&id=113458
//	 * @throws RemoteException 
//	 */
//	@Test
//	public void testGetAphiarecords() throws RemoteException {
//		long start = System.currentTimeMillis();
//		AphiaRecord [] result = wDAO.getAphiaRecordsByName("Neogloboquadrina pachyderma", true);
//		long stop = System.currentTimeMillis();
//		System.out.println("time in seconds needed to get a single result from the worms WS " +((float)(stop-start)/1000) +"s");
//		System.out.println(result[0].getScientificname());
//		assertTrue(result.length > 0);
//		assertTrue(result[0].getAphiaID() == 113458);
//		assertTrue(result[0].getFamily().equals("Globorotaliidae"));
//		assertTrue(result[0].getGenus().equals("Neogloboquadrina"));
//		assertTrue(result[0].get_class().equals("Globothalamea"));
//		
//		//new Aphia status
//		AphiaRecord q = wDAO.getAphiaRecordByID(641392);
//		System.out.println(q.getStatus());
//		assertTrue(q.getAphiaID() == 641392);
//		q = wDAO.getAphiaRecordByID(700724200);
//		assertNull(q);
//		Source [] check = wDAO.getSourcesByAphiaID(113458);
//		
//		//here we have a duplicate entry
//		//http://www.marinespecies.org/aphia.php?p=taxdetails&id=112208
//		start = System.currentTimeMillis();
//		result = wDAO.getAphiaRecordsByName("Neogloboquadrina", true);
//		stop = System.currentTimeMillis();
//		System.out.println("time in seconds needed to get this result SET from the worms WS " +((float)(stop-start)/1000) +"s");
//		
//		for (AphiaRecord ar : result) {
//			System.out.println(ar.getAphiaID());
//			System.out.println(ar.getStatus());
//			System.out.println(ar.getUnacceptreason());
//			System.out.println(ar.getValid_name());
//			System.out.println(ar.getScientificname());
//		}
//		
//		assertTrue(String.valueOf(result.length), result.length == 9);
//		
//		//http://www.marinespecies.org/aphia.php?p=taxdetails&id=112206
//		start = System.currentTimeMillis();
//		result = wDAO.getAphiaRecordsByName("Globigerinita", true);
//		stop = System.currentTimeMillis();
//		System.out.println("time in seconds needed to get a single result from the worms WS " +((float)(stop-start)/1000) +"s");
//		System.out.println(result[0].getScientificname());
//		assertTrue(result[0].getAphiaID() == 112206);
//		
//		//here we get a collection, because Globorotalia is a genus level
//		result = wDAO.getAphiaRecordsByName("Globorotalia", true);
//		assertTrue(result.length > 0);
//	
//	}
//	
//	@Test
//	public void testGetAphiaRecordNoLikeSearch() {
//		WORMSWSDAO wDAO2 = new WORMSWSDAO();
//		AphiaRecord [] result = wDAO.getAphiaRecordsByName("Neogloboquadrina pachyderma", false);
//		System.out.println(result[0].getScientificname());
//		assertTrue(result.length == 1);
//		assertTrue(result[0].getAphiaID() == 113458);
//		assertTrue(result[0].getFamily().equals("Globorotaliidae"));
//		assertTrue(result[0].getGenus().equals("Neogloboquadrina"));
//		assertTrue(result[0].get_class().equals("Globothalamea"));
//		
//		
//		result = wDAO.getAphiaRecordsByName("Globigerinoides", false);
//		System.out.println(result[0].getScientificname());
//		assertTrue(result.length == 1);
//		
//		result = wDAO.getAphiaRecordsByName("Globigerina calida", false);
//		assertTrue(result.length == 1);
//	}
//	
//	@Test
//	public void testGetAphiaRecordsByNameslist() {
//		AphiaRecord[][] result = new AphiaRecord[0][0]; 
//		String [] scientificnames = {"Neogloboquadrina pachyderma", "Globigerinoides", "Globorotalia"};
//		result = wDAO.getAphiaRecordsByNamelist(scientificnames, true);
//		assertTrue(result.length == 3);
//		System.out.println(result.length);
//		assertTrue(result[2].length == 50); 
//	}
//	
//	@Test
//	public void testAphiaDataCaseAnomalina() {
//		
//		
//		AphiaRecord [] result = wDAO.getAphiaRecordsByName("Anomalina", false);
//		assertTrue(result.length == 1);
//		
//		
//		System.out.println(result[0].getScientificname());
//		System.out.println(result[0].getMatch_type());
//		System.out.println(result[0].getValid_AphiaID());
//		System.out.println(result[0].getValid_name());
//		System.out.println(result[0].getUnacceptreason());
//		
//		result = wDAO.getAphiaRecordsByName("Anomalina", true);
//															//this value changed very often
//		assertTrue(String.valueOf(result.length), result.length == 50);
//		
//		result = wDAO.getAphiaRecordsByName("test no match", false);
//		assertNull(result);
//
//	}
//	
//	@Test
//	public void testClassificationsInfo() throws RemoteException {
//		WORMSWSServiceIMPL wormswsServiceIMPL = new WORMSWSServiceIMPL();
//		Classification classify;
//		
//		System.out.println(wDAO.getServiceAdress());
//		System.out.println(wormswsServiceIMPL.getDescription());
//		System.out.println("***************** aphia record infos Formaminifera **************");
//		//Foraminifera 
//		AphiaRecord [] ar = wormswsServiceIMPL.getWSDAO().getAphiaRecordsByName("Foraminifera", false);
//		System.out.println(ar[0].getAphiaID());
//		System.out.println(ar[0].getRank());
//		System.out.println(ar[0].getFamily());
//		System.out.println(ar[0].getGenus());
//		System.out.println(ar[0].getKingdom());
//		System.out.println(ar[0].getOrder());
//		System.out.println(ar[0].getPhylum());
//		System.out.println(ar[0].getIsExtinct());
//		System.out.println(ar[0].getScientificname());
//		
//		
//		
//		System.out.println("***************** classify Foraminifera**************");
//		classify = wormswsServiceIMPL.getWSDAO().getAphiaClassificationByID(ar[0].getAphiaID());
////		System.out.println(classify.getAphiaID());
////		System.out.println(classify.getRank());
////		System.out.println(classify.getScientificname());
////		System.out.println("childlevel");
////		System.out.println(classify.getChild().getRank());
////		System.out.println(classify.getChild().getScientificname());
//		System.out.println("***************** classify **************");
//		while(classify.getChild() !=null) {
//			System.out.println(classify.getAphiaID());
//			System.out.println(classify.getRank());
//			System.out.println(classify.getScientificname());
//			System.out.println("next level");
//			classify = classify.getChild();
//			
//		}
//		
//		
//		System.out.println("***************** aphia record infos , Neogloboquadrina pachyderma**************");
//		
//		ar = wormswsServiceIMPL.getWSDAO().getAphiaRecordsByName("Neogloboquadrina pachyderma", false);
//		System.out.println(ar[0].getRank());
//		System.out.println(ar[0].getFamily());
//		System.out.println(ar[0].getGenus());
//		System.out.println(ar[0].getKingdom());
//		System.out.println(ar[0].getOrder());
//		System.out.println(ar[0].getPhylum());
//		System.out.println(ar[0].getIsExtinct());
//	
//		classify = wormswsServiceIMPL.getWSDAO().getAphiaClassificationByID(ar[0].getAphiaID());
////		System.out.println(classify.getAphiaID());
////		System.out.println(classify.getRank());
////		System.out.println(classify.getScientificname());
////		System.out.println("childlevel");
////		System.out.println(classify.getChild().getRank());
////		System.out.println(classify.getChild().getScientificname());
//		System.out.println("***************** classify **************");
//		while(classify.getChild() !=null) {
//			System.out.println(classify.getAphiaID());
//			System.out.println(classify.getRank());
//			System.out.println(classify.getScientificname());
//			System.out.println("next level");
//			classify = classify.getChild();
//			
//		}
//		System.out.println("***************** classify, Globorotalia anfracta, 113447 **************");
//		//Globorotalia anfracta, 113447
//		ar = wormswsServiceIMPL.getWSDAO().getAphiaRecordsByName("Globorotalia anfracta", false);
//		assertTrue(ar[0].getAphiaID()==113447);
//		classify = wormswsServiceIMPL.getWSDAO().getAphiaClassificationByID(113447);
//		
//		while(classify.getChild() !=null) {
//			System.out.println(classify.getAphiaID());
//			System.out.println(classify.getRank());
//			System.out.println(classify.getScientificname());
//			//System.out.println(wDAO.g);
//			System.out.println("next level");
//			classify = classify.getChild();
//			
//		}
//		
//		System.out.println("***************** classify, case subspecies 582506 **************");
//		//case subspecies 582506
//		AphiaRecord arS = wormswsServiceIMPL.getWSDAO().getAphiaRecordByID(582506);
//		System.out.println(arS.getRank());
//		System.out.println(arS.getPhylum());
//		classify = wormswsServiceIMPL.getWSDAO().getAphiaClassificationByID(582506);
//		
//		while(classify.getChild() !=null) {
//			System.out.println(classify.getAphiaID());
//			System.out.println(classify.getRank());
//			System.out.println(classify.getScientificname());
//			//System.out.println(wDAO.g);
//			System.out.println("next level");
//			classify = classify.getChild();
//		}
//		
//		classify = wDAO.getAphiaClassificationByID(710500);
//		System.out.println("***************** classify varity **************");
//		while(classify.getChild() !=null) {
//			System.out.println(classify.getAphiaID());
//			System.out.println(classify.getRank());
//			System.out.println(classify.getScientificname());
//			System.out.println("next level");
//			classify = classify.getChild();
//			
//		}
//		System.out.println("***************** classify biota - special case - no childs **************");
//		//biota 
//		classify = wDAO.getAphiaClassificationByID(1);
//		assertTrue(Integer.toString(classify.getChild().getAphiaID()),classify.getChild().getAphiaID() == 0);
//	}
//	
//	//Lagena
//	@Test
//	public void testHandlingHomonym(){
//		//case Lagena
//		AphiaRecord [] ars = wDAO.getAphiaRecordsByName("Lagena", false);
//		assertTrue(ars.length == 3);
//		
//		//case of deleted records ...
//		AphiaRecord [] ar2 = wDAO.getAphiaRecordsByName("Dendrophyra", false);
//		
//		//valid entry 465860
//		AphiaRecord ar3 = wDAO.getAphiaRecordByID(465860);
//		
//		AphiaRecord ar4 = wDAO.getAphiaRecordByID(711491);
//		
//		
//	}
//	
//	@Test
//	public void testCheckSpecialCases() throws RemoteException {
//		//Cassidulina laevigata , its a homonym case ? first result is valid, second is nonvalid, both have the same name
//		AphiaRecord [] ars = wDAO.getAphiaRecordsByName("Cassidulina laevigata", false);
//		assertTrue(ars.length ==2);
//		assertTrue(ars[0].getAphiaID() == 113077);
//		assertTrue(ars[0].getAphiaID() == ars[0].getValid_AphiaID());
//		assertTrue(ars[0].getScientificname().equals(ars[1].getScientificname()));
//		
//		Classification classify = wDAO.getAphiaClassificationByID(113077);
//		System.out.println("***************** classify Cassidulina laevigata valid aphia entity **************");
//		while(classify.getChild() !=null) {
//			System.out.println(classify.getAphiaID());
//			System.out.println(classify.getRank());
//			System.out.println(classify.getScientificname());
//			System.out.println("next level");
//			classify = classify.getChild();
//			
//		}
//		
//		classify = wDAO.getAphiaClassificationByID(526180);
//		System.out.println("***************** classify Cassidulina laevigata non valid aphia entity **************");
//		while(classify.getChild() !=null) {
//			System.out.println(classify.getAphiaID());
//			System.out.println(classify.getRank());
//			System.out.println(classify.getScientificname());
//			System.out.println("next level");
//			classify = classify.getChild();
//			
//		}
//	}
//
//	
//	@Test
//	public void testCheckWoRMSStati() {
//		//case nomen dubium, with no valid aphiaID
//		AphiaRecord [] result = wDAO.getAphiaRecordsByName("Valvulina austriaca", false);
//		//at round 7.4.14 here we got NULL
//		//assertNull(result[0].getValid_AphiaID());
//		assertTrue(result[0].getValid_AphiaID()==0);
//	}
//	
//	@Test
//	public void testSpecialCaseTribrachia() {
//		AphiaRecord [] result = wDAO.getAphiaRecordsByName("Tribrachia", false);
//		assertTrue(result.length > 0);
//	}
//	
//	/**Attention
//	 * not all WoRMS entity have a vernacular .. its normal because the folks
//	 * around the world doesnt know for example all kind of foraminiferas
//	 */
//	@Test
//	public void checkVernacular() {
//		//no vernacular
//		Vernacular [] v  = wDAO.getVernacularByID(106388);
//		assertNull(v);
//		
//		//weisser Thun, 126065 
//		v  = wDAO.getVernacularByID(126065);
//		System.out.println(v[0].getVernacular());
//		assertTrue(v.length > 0);
//		
//	}
//	
//	@Test
//	public void testVariety() {
//		//Globorotalia menardii
//		AphiaRecord [] result = wDAO.getAphiaRecordsByName("Globorotalia menardii", false);
//		assertTrue(result.length > 0);
//	}
//
//	//@Test
//	public void testFetchChilds() {
//		//Anomalina d'Orbigny, 1826 
//		AphiaRecord [] childs = wDAO.getChildsByAphiaID(112081 , 1);
//		//the sum changes - increases, so maybe not good for testing
//		assertTrue(String.valueOf(childs.length),childs.length == 49);
//		
//		//Globigerinina AphiaID: 414700 
//		childs = wDAO.getChildsByAphiaID(414700 , 1);
//		assertTrue(childs.length == 8);
//		
//		//Bulimina d'Orbigny, 1826 AphiaID: 11211, 129
//		childs = wDAO.getChildsByAphiaID(112110 , 1);
//		assertTrue(String.valueOf(childs.length),childs.length == 50);
//		childs = wDAO.getChildsByAphiaID(112110 , 51);
//		assertTrue(String.valueOf(childs.length),childs.length == 50);
//		childs = wDAO.getChildsByAphiaID(112110 , 101);
//		assertTrue(String.valueOf(childs.length),childs.length == 31);
//	}
//	
//	//@Test
//	public void testWSConnection() throws IOException {
//		assertTrue(wDAO.checkWebserviceConnection());
//	}
//	
//	@Test
//	public void testNPEHandling() {
//		AphiaRecord [] test = wDAO.getAphiaRecordsByName("Quercus Robur", false);
//		assertNull(test);
//
//	}
//	
//	@Test
//	public void DAOSynonymsTest() {
//	    AphiaRecord[] check = wDAO.getAphiaSynonymsByID(163158);
//	    assertNotNull(check);
//	    assertTrue(check[0].getAphiaID() == 414703);
//	}
//	   @Test
//	    public void DAOClassifyTest() {
//	        Classification check = null;
//            try {
//                check = wDAO.getAphiaClassificationByID(163158);
//            } catch (RemoteException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//	        assertNotNull(check);
//	    }
//	/**
//	 * a error at worms
//	 * FIXME figure out this handling
//	 */
//	//@Test
//	public void testCaseZealandica() {
//		AphiaRecord [] test = wDAO.getAphiaRecordsByName("%zealandica", false);
//		//assertTrue(test.length > 0);
//		
//		assertNull(test);
//	}
//	
//	
//	@Test
//	public void testAnimalClassify() {
//		
//		WORMSWSServiceIMPL wormswsServiceIMPL = new WORMSWSServiceIMPL();
//		Classification classify = null;
//		System.out.println("***************** classify, 272925 ************** superclass is 2 times related");
//		//Globorotalia anfracta, 113447
//
//		try {
//			classify = wormswsServiceIMPL.getWSDAO().getAphiaClassificationByID(272925);
//		} catch (RemoteException e) {
//			
//			e.printStackTrace();
//		}
//		
//		while(classify.getChild() !=null) {
//			System.out.println(classify.getAphiaID());
//			System.out.println(classify.getRank());
//			System.out.println(classify.getScientificname());
//			//System.out.println(wDAO.g);
//			System.out.println("next level");
//			classify = classify.getChild();
//			
//		}
//	}
//    @Test
//    public void testSource() {
//        
//        WORMSWSServiceIMPL wormswsServiceIMPL = new WORMSWSServiceIMPL();
//        Source[] s = null;
//        try {
//           s= wormswsServiceIMPL.wormsWSDAO.getSourcesByAphiaID(113463);
//        } catch (RemoteException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }
}

package de.externalwebservices.worms.aphiav1_0;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class SpeedTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void ClassifyEntitySpeedTest() {
		System.out.println("Classification entity speed");
		WORMSWSDAO wormswsdao = new WORMSWSDAO();
		int i = 0;
		long average = 0;
		for(int aid : aphiaIDs) {
			long start = System.currentTimeMillis();
		try {
			wormswsdao.getAphiaClassificationByID(aid);
		} catch (RemoteException e) {
			
			e.printStackTrace();
		}
		long stop = System.currentTimeMillis();
		long duration = stop-start;
		average += duration;
		System.out.println(duration + " ms");
		i++;
		}
		
		System.out.println("average " + (average / i)+" ms " +i +" " +aphiaIDs.length);
	}
	
	@Test
	public void entitySpeedtest() {
		System.out.println("Aphia entity speed");
		WORMSWSDAO wormswsdao = new WORMSWSDAO();
		int i = 0;
		long average = 0;
		for(int aid : aphiaIDs) {
			long start = System.currentTimeMillis();

			wormswsdao.getAphiaRecordByID(aid);

		long stop = System.currentTimeMillis();
		long duration = stop-start;
		average += duration;
		System.out.println(duration + " ms");
		i++;
		}
		
		System.out.println("average " + (average / i)+" ms " +i +" " +aphiaIDs.length);
	}

	static final int [] aphiaIDs = {757089,
		754606,
		754605,
		754435,
		754253,
		754252,
		742429,
		740456,
		740454,
		739490,
		739485,
		739484,
		739382,
		739380,
		739357,
		739349,
		739342,
		739341,
		739331,
		739164,
		739163,
		739159,
		739158,
		739151,
		739150,
		739145,
		739142,
		739130,
		738934,
		738658};
 	
}

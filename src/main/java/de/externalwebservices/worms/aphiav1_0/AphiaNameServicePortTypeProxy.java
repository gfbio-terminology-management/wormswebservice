package de.externalwebservices.worms.aphiav1_0;

public class AphiaNameServicePortTypeProxy implements de.externalwebservices.worms.aphiav1_0.AphiaNameServicePortType {
	private String _endpoint = null;
	private de.externalwebservices.worms.aphiav1_0.AphiaNameServicePortType aphiaNameServicePortType = null;

	public AphiaNameServicePortTypeProxy() {
		_initAphiaNameServicePortTypeProxy();
	}

	public AphiaNameServicePortTypeProxy(String endpoint) {
		_endpoint = endpoint;
		_initAphiaNameServicePortTypeProxy();
	}

	private void _initAphiaNameServicePortTypeProxy() {
		try {
			aphiaNameServicePortType = (new de.externalwebservices.worms.aphiav1_0.AphiaNameServiceLocator())
					.getAphiaNameServicePort();
			if (aphiaNameServicePortType != null) {
				if (_endpoint != null)
					((javax.xml.rpc.Stub) aphiaNameServicePortType)
							._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
				else
					_endpoint = (String) ((javax.xml.rpc.Stub) aphiaNameServicePortType)
							._getProperty("javax.xml.rpc.service.endpoint.address");
			}

		} catch (javax.xml.rpc.ServiceException serviceException) {
		}
	}

	public String getEndpoint() {
		return _endpoint;
	}

	public void setEndpoint(String endpoint) {
		_endpoint = endpoint;
		if (aphiaNameServicePortType != null)
			((javax.xml.rpc.Stub) aphiaNameServicePortType)._setProperty("javax.xml.rpc.service.endpoint.address",
					_endpoint);

	}

	public de.externalwebservices.worms.aphiav1_0.AphiaNameServicePortType getAphiaNameServicePortType() {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType;
	}

	public int getAphiaID(java.lang.String scientificname, boolean marine_only) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaID(scientificname, marine_only);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord[] getAphiaRecords(java.lang.String scientificname,
			boolean like, boolean fuzzy, boolean marine_only, int offset) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaRecords(scientificname, like, fuzzy, marine_only, offset);
	}

	public java.lang.String getAphiaNameByID(int aphiaID) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaNameByID(aphiaID);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord getAphiaRecordByID(int aphiaID)
			throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaRecordByID(aphiaID);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord[] getAphiaRecordsByIDs(int[] aphiaids)
			throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaRecordsByIDs(aphiaids);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord getAphiaRecordByExtID(java.lang.String id,
			java.lang.String type) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaRecordByExtID(id, type);
	}

	public java.lang.String[] getExtIDbyAphiaID(int aphiaID, java.lang.String type) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getExtIDbyAphiaID(aphiaID, type);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord[][] getAphiaRecordsByNames(
			java.lang.String[] scientificnames, boolean like, boolean fuzzy, boolean marine_only)
			throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaRecordsByNames(scientificnames, like, fuzzy, marine_only);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord[] getAphiaRecordsByVernacular(java.lang.String vernacular,
			boolean like, int offset) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaRecordsByVernacular(vernacular, like, offset);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord[] getAphiaRecordsByDate(java.lang.String startdate,
			java.lang.String enddate, boolean marine_only, int offset) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaRecordsByDate(startdate, enddate, marine_only, offset);
	}

	public de.externalwebservices.worms.aphiav1_0.Classification getAphiaClassificationByID(int aphiaID)
			throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaClassificationByID(aphiaID);
	}

	public de.externalwebservices.worms.aphiav1_0.Source[] getSourcesByAphiaID(int aphiaID)
			throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getSourcesByAphiaID(aphiaID);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord[] getAphiaSynonymsByID(int aphiaID, int offset)
			throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaSynonymsByID(aphiaID, offset);
	}

	public de.externalwebservices.worms.aphiav1_0.Vernacular[] getAphiaVernacularsByID(int aphiaID)
			throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaVernacularsByID(aphiaID);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord[] getAphiaChildrenByID(int aphiaID, int offset,
			boolean marine_only) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaChildrenByID(aphiaID, offset, marine_only);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord[][] matchAphiaRecordsByNames(
			java.lang.String[] scientificnames, boolean marine_only) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.matchAphiaRecordsByNames(scientificnames, marine_only);
	}

	public de.externalwebservices.worms.aphiav1_0.Distribution[] getAphiaDistributionsByID(int aphiaID)
			throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaDistributionsByID(aphiaID);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRank[] getAphiaTaxonRanksByID(int taxonRankID, int aphiaID)
			throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaTaxonRanksByID(taxonRankID, aphiaID);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRank[] getAphiaTaxonRanksByName(java.lang.String taxonRank,
			int aphiaID) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaTaxonRanksByName(taxonRank, aphiaID);
	}

	public de.externalwebservices.worms.aphiav1_0.AphiaRecord[] getAphiaRecordsByTaxonRankID(int taxonRankID,
			int belongsTo, int offset) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaRecordsByTaxonRankID(taxonRankID, belongsTo, offset);
	}

	public de.externalwebservices.worms.aphiav1_0.AttributeKey[] getAphiaAttributeKeysByID(int id,
			boolean include_children) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaAttributeKeysByID(id, include_children);
	}

	public de.externalwebservices.worms.aphiav1_0.AttributeValue[] getAphiaAttributeValuesByCategoryID(int id)
			throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaAttributeValuesByCategoryID(id);
	}

	public int[] getAphiaIDsByAttributeKeyID(int id, int offset) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaIDsByAttributeKeyID(id, offset);
	}

	public de.externalwebservices.worms.aphiav1_0.Attribute[] getAphiaAttributesByAphiaID(int id,
			boolean include_inherited) throws java.rmi.RemoteException {
		if (aphiaNameServicePortType == null)
			_initAphiaNameServicePortTypeProxy();
		return aphiaNameServicePortType.getAphiaAttributesByAphiaID(id, include_inherited);
	}

}
package de.externalwebservices.worms.aphiav1_0;

import java.io.IOException;
import java.rmi.RemoteException;
import javax.xml.rpc.ServiceException;
import org.apache.log4j.Logger;

public class WORMSWSDAO {

  private static final Logger LOGGER = Logger.getLogger(WORMSWSDAO.class);
  private AphiaNameServiceLocator locator;
  private AphiaNameServicePortType port;
  public final int waitTime = 1000;

  public WORMSWSDAO() {
    init();
  }

  private void init() {

    locator = new AphiaNameServiceLocator();
    try {
      port = locator.getAphiaNameServicePort();

    } catch (ServiceException e) {

      e.printStackTrace();
    }
  }

  public void reInitSOAP() {
    init();
  }

  public String getServiceAdress() {

    return locator.getAphiaNameServicePortAddress();
  }

  /*
   * (non-Javadoc)
   * 
   * @see AphiaNameServicePortType.getAphiaRecords
   * 
   * actually here we use : name, INPUT, true, false, 0);
   */
  /**
   * worms returns NULL if no match ....
   * 
   * @param scientificname
   * @param likeSearch
   * @return a result with aphia data, or null if no match, empty if connection was broken
   */
  public AphiaRecord[] getAphiaRecordsByName(String scientificname, boolean likeSearch) {
    // AphiaRecord [] result = new AphiaRecord[0];

    for (int tries = 0;; tries++) {
      try {
        return port.getAphiaRecords(scientificname, likeSearch, false, false, 0);
      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, WoRMS to lame " + scientificname, re);
          return null; // dont return empty because the port returns also null and
          // the next filter step checks for null
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + scientificname);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupt " + scientificname, e);

        }
      }
    }
  }

  /**
   * 
   * @param aphiaID
   * @return a result with aphia data, or null if no match, empty if connection was broken
   */
  public AphiaRecord getAphiaRecordByID(int aphiaID) {

    for (int tries = 0;; tries++) {
      try {
        return this.port.getAphiaRecordByID(aphiaID);
      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, WoRMS to lame " + aphiaID, re);
          return new AphiaRecord();
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + aphiaID);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupt " + aphiaID, e);

        }
      }
    }
  }

  /**
   * 
   * @param aphiaID
   * @return a result with aphia children data, or null if no match, empty if connection was broken
   */
  public AphiaRecord[] getAphiaChildrenByID(int aphiaID) {

    for (int tries = 0;; tries++) {
      try {
        return this.port.getAphiaChildrenByID(aphiaID, 0, false);
      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, WoRMS to lame " + aphiaID, re);
          return null;
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + aphiaID);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupt " + aphiaID, e);

        }
      }
    }
  }

  /**
   * 
   * @param aphiaID
   * @return a result with synonym data, or null if no match, empty if connection was broken
   */
  public AphiaRecord[] getAphiaSynonymsByID(int aphiaID) {

    for (int tries = 0;; tries++) {
      try {
        return this.port.getAphiaSynonymsByID(aphiaID, 1);
      } catch (RemoteException re) {
        if (tries >= 3) {
          AphiaRecord[] result = new AphiaRecord[0];
          LOGGER.warn("match failed, WoRMS to lame " + aphiaID, re);
          return result;
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + aphiaID);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupt " + aphiaID, e);

        }
      }
    }
  }

  /**
   * 
   * @param scientificnames
   * @return
   */
  public AphiaRecord[][] getAphiaRecordsByNamelist(String[] scientificnames, boolean likeSearch) {

    AphiaRecord[][] result = new AphiaRecord[0][0];

    try {
      result = port.getAphiaRecordsByNames(scientificnames, likeSearch, true, false);
    } catch (RemoteException re) {
      LOGGER.warn("match failed " + re);

    }

    if (result == null) {
      result = new AphiaRecord[0][0];
    }
    return result;
  }

  public AphiaRecord[][] fuzzyWORMSSearch(String[] scientificnames, boolean marineOnly) {
    AphiaRecord[][] result = new AphiaRecord[0][0];
    try {
      result = port.matchAphiaRecordsByNames(scientificnames, marineOnly);
    } catch (RemoteException re) {

      LOGGER.warn("match failed " + re);
    }
    if (result == null) {
      result = new AphiaRecord[0][0];
    }
    return result;
  }

  /**
   * get Sources like papers and so on
   * 
   * @param aphiaID
   * @return
   * @throws RemoteException
   */
  public Source[] getSourcesByAphiaID(int aphiaID) throws RemoteException {
    return port.getSourcesByAphiaID(aphiaID);
  }

  /**
   * this is also the method to get the broader terms, WoRMS deliver a classifiation object
   * container which iterate from top to the level of the given input aphia id
   * 
   * @param aphiaID
   * @return
   * @throws RemoteException
   */
  public Classification getAphiaClassificationByID(int aphiaID) throws RemoteException {
    return port.getAphiaClassificationByID(aphiaID);
  }

  @Deprecated
  public boolean checkWebserviceConnection() throws IOException {
    // FIXME getLastModified funktioniert nicht und leifert einen Nullpointer
    // long test = this.locator.getWSDLDocumentLocation().openConnection().getLastModified();

    boolean result =
        this.locator.getWSDLDocumentLocation().openConnection().getLastModified() != 0 ? true
            : false;
    // Debug
    // System.out.println(this.locator.getWSDLDocumentLocation().openConnection().getLastModified());
    return result;
  }

  public Vernacular[] getVernacularByID(int aphiaID) {
    Vernacular[] result = null;
    try {
      result = this.port.getAphiaVernacularsByID(aphiaID);
    } catch (RemoteException re) {

      LOGGER.warn("match failed " + re);
    }
    return result;
  }

  public AphiaRecord[] getVernacularByName(String name, boolean likeSearch) {
    AphiaRecord[] result = null;
    try {
      result = this.port.getAphiaRecordsByVernacular(name, likeSearch, 0);
    } catch (RemoteException re) {

      LOGGER.warn("match failed " + re);
    }
    return result;
  }

  /**
   * Lists all AphiaRecords (taxa) that have their last edit action (modified or added) during the
   * specified period
   * 
   * @param startDate ISO 8601 formatted start date(time). Default=today(). i.e.
   *        2020-11-19T09:23:11+00:00
   * @param endDate ISO 8601 formatted end date(time). Default=today().i.e.
   *        2020-11-19T09:23:11+00:00
   * @param marine_only Limit to marine taxa. Default=true
   * @param offset Starting recordnumber, when retrieving next chunk of (50) records. Default=0
   * @return AphiaRecords
   */
  public AphiaRecord[] getAphiaRecordsByDate(String startDate, String endDate) {
    LOGGER.debug("startDate: " + startDate);
    LOGGER.debug("endDate: " + endDate);


    try {
      return port.getAphiaRecordsByDate(startDate, endDate, true, 0);
    } catch (RemoteException e) {
      LOGGER.error(e.getMessage());
    }

    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.externalwebservices.worms.aphiav1_0.AphiaNameServicePortType# getAphiaChildrenByID(int,
   * int, boolean)
   * 
   * here we fetch without a "marine only filter"
   * 
   */
  public AphiaRecord[] getChildsByAphiaID(int parentAphiaID, int offset) {

    for (int tries = 0;; tries++) {
      try {
        return this.port.getAphiaChildrenByID(parentAphiaID, offset, false);
      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, WoRMS to lame " + parentAphiaID, re);
          return new AphiaRecord[0];
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + parentAphiaID);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupt " + parentAphiaID, e);

        }
      }
    }

  }

  @Override
  protected void finalize() {
    System.gc();
  }
}

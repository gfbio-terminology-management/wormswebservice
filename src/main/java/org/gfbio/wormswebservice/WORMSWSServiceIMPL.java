package org.gfbio.wormswebservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import javax.xml.rpc.ServiceException;
import org.apache.log4j.Logger;
import org.gfbio.config.WSConfiguration;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.resultset.AllBroaderResultSetEntry;
import org.gfbio.resultset.CapabilitiesResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.HierarchyResultSetEntry;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.gfbio.util.YAMLConfigReader;
import de.externalwebservices.worms.aphiav1_0.AphiaNameServiceLocator;
import de.externalwebservices.worms.aphiav1_0.AphiaNameServicePortType;
import de.externalwebservices.worms.aphiav1_0.AphiaRecord;
import de.externalwebservices.worms.aphiav1_0.Classification;
import de.externalwebservices.worms.aphiav1_0.Source;
import de.externalwebservices.worms.aphiav1_0.Vernacular;
import de.externalwebservices.worms.aphiav1_0.WORMSWSDAO;

/**
 * Webservice implementation in the GFBio Terminology Service. This class implements all necessary
 * methods for the TS endpoints according to the WSInterface defined in the gfbioapi project.
 * 
 * @author sbode <A HREF="mailto:sbode@marum.de">Steffen Bode</A>
 * 
 */
public class WORMSWSServiceIMPL implements WSInterface {

  private static final Logger LOGGER = Logger.getLogger(WORMSWSServiceIMPL.class);

  public final String wormsTaxonLSID = "urn:lsid:marinespecies.org:taxname:";
  public final String wormsTaxonURL = "http://www.marinespecies.org/aphia.php?p=taxdetails&id=";

  private WORMSWSDAO wormsWSDAO;
  private WSConfiguration wormsConfig;

  public WORMSWSServiceIMPL() {
    wormsWSDAO = new WORMSWSDAO();

    YAMLConfigReader reader = YAMLConfigReader.getInstance();
    wormsConfig = reader.getWSConfig("WORMS");

    LOGGER.info(" a WoRMS webservice is ready to use");
  }

  @Override
  public String getDescription() {
    return wormsConfig.getDescription();
  }

  @Override
  public String getName() {
    return wormsConfig.getName();
  }

  @Override
  public String getAcronym() {
    return wormsConfig.getAcronym();
  }

  public String getURI() {

    // FIXME 11/05/20 correct URI?
    return wormsConfig.getUri();

    // return this.wormsWSDAO.getServiceAdress(); //
    // this.wormsWSDAO.locator.getWSDLDocumentLocation().toURI().toString();
  }

  public List<AphiaRecord> getAphiarecordsByIDList(List<Integer> idList) {
    List<AphiaRecord> result = new ArrayList<>(idList.size());
    for (int id : idList) {
      AphiaRecord ar = this.wormsWSDAO.getAphiaRecordByID(id);
      if (ar != null) {
        result.add(ar);

      }
    }
    return result;
  }

  @Override
  public boolean supportsMatchType(String match_type) {
    return Arrays.stream(wormsConfig.getSearchModes())
        .anyMatch(SearchModes.valueOf(match_type)::equals);
  }


  @Override
  public List<String> getDomains() {
    List<String> l = new ArrayList<String>();
    for (Domains d : wormsConfig.getWsDomains()) {
      l.add(d.name());
    }
    return l;
  }


  @Override
  public String getCurrentVersion() {

    return wormsConfig.getVersion();
  }

  /*
   * actually only exact and like search is supported (non-Javadoc)
   * 
   * @see org.gfbio.terminologies.interfaces.WSInterface#search(java.lang.String, java.lang.String)
   */
  /**
   * Implements the search method of the WSInterface Searches for names and common names in the
   * database based on the match type "exact" or "included"
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SearchResultSetEntry> search(String query, String match_type) {
    LOGGER.info("Search query " + query + " matchtype " + match_type);
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("worms");
    if (match_type.equals(SearchTypes.exact.toString())) {
      // Scientific Name Search
      AphiaRecord[] scNa = this.wormsWSDAO.getAphiaRecordsByName(query, false);
      if (scNa != null) {
        for (AphiaRecord ar : scNa) {
          setEntry(rs, ar);
        }
      }
      // Common Names Search
      AphiaRecord[] veNa = this.wormsWSDAO.getVernacularByName(query, false);
      if (veNa != null) {
        HashSet<AphiaRecord> usedRecords = new HashSet<AphiaRecord>();
        for (AphiaRecord ar : veNa) {
          if (!usedRecords.contains(ar)) {
            setEntry(rs, ar);
          }
          usedRecords.add(ar);
        }
      }
    }
    if (match_type.equals(SearchTypes.included.toString())) {
      // Scientific Name Search
      AphiaRecord[] scNa = this.wormsWSDAO.getAphiaRecordsByName(query, true);
      if (scNa != null) {
        for (AphiaRecord ar : scNa) {
          setEntry(rs, ar);
        }
      }
      // Common Names Search
      AphiaRecord[] veNa = this.wormsWSDAO.getVernacularByName(query, true);
      if (veNa != null) {
        HashSet<AphiaRecord> usedRecords = new HashSet<AphiaRecord>();
        for (AphiaRecord ar : veNa) {
          if (!usedRecords.contains(ar)) {
            setEntry(rs, ar);
          }
          usedRecords.add(ar);
        }
      }
    }
    LOGGER.info("search query processed " + query + " matchtype " + match_type);
    return rs;
  }

  /*
   * this delivers in this order: Top -> Down, means superdomain to the adressed level, so at [0] is
   * always the superdomain (non-Javadoc)
   * 
   * @see org.gfbio.interfaces.WSInterface#getAllBroader(java.lang.String)
   */
  /**
   * Implements the getAllBroader method of the WSInterface Gets all broader terms of a taxon given
   * its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<AllBroaderResultSetEntry> getAllBroader(String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(wormsTaxonURL)) {
      externalID = externalID.substring(wormsTaxonURL.length());
    }
    if (uriOrexternalID.startsWith(wormsTaxonLSID)) {
      externalID = externalID.substring(wormsTaxonLSID.length());
    }
    GFBioResultSet<AllBroaderResultSetEntry> rs =
        new GFBioResultSet<AllBroaderResultSetEntry>("worms");
    Classification taxPath = null;
    try {
      taxPath = this.wormsWSDAO.getAphiaClassificationByID(Integer.valueOf(externalID));

    } catch (NumberFormatException e) {
      LOGGER.error(e);
    } catch (RemoteException e) {
      LOGGER.error(e);
    }

    if (taxPath != null) {
      while (taxPath.getChild().getChild() != null) {
        setEntry(rs, taxPath);
        taxPath = taxPath.getChild();

      }
    } else {
      LOGGER.error(" No classification for this id  " + externalID);
      return null;
    }
    return rs;
  }

  /**
   * Implements the getSynonyms method of the WSInterface Gets the list of synonyms of a taxon given
   * its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SynonymsResultSetEntry> getSynonyms(final String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(wormsTaxonURL)) {
      externalID = externalID.substring(wormsTaxonURL.length());
    }
    if (uriOrexternalID.startsWith(wormsTaxonLSID)) {
      externalID = externalID.substring(wormsTaxonLSID.length());
    }
    int aphiaID = Integer.valueOf(externalID);
    GFBioResultSet<SynonymsResultSetEntry> rs = new GFBioResultSet<SynonymsResultSetEntry>("worms");
    AphiaRecord[] synonymList = null;
    try {
      synonymList = this.wormsWSDAO.getAphiaSynonymsByID(aphiaID);
    } catch (NumberFormatException e) {
      LOGGER.error(e);
    }

    if (synonymList != null && synonymList.length != 0) {
      setEntry(rs, synonymList);
    } else {
      LOGGER.error(" No synonyms for this id  " + externalID);
    }
    return rs;
  }

  /**
   * Implements the getHierarchy method of the WSInterface Gets the broader hierarchy of a taxon
   * given its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<HierarchyResultSetEntry> getHierarchy(String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(wormsTaxonURL)) {
      externalID = externalID.substring(wormsTaxonURL.length());
    }
    if (uriOrexternalID.startsWith(wormsTaxonLSID)) {
      externalID = externalID.substring(wormsTaxonLSID.length());
    }
    GFBioResultSet<HierarchyResultSetEntry> rs =
        new GFBioResultSet<HierarchyResultSetEntry>("worms");
    Classification taxPath = null;
    try {
      taxPath = this.wormsWSDAO.getAphiaClassificationByID(Integer.valueOf(externalID));
    } catch (NumberFormatException e) {
      LOGGER.error(e);
    } catch (RemoteException e) {
      LOGGER.error(e);
    }
    Classification parent = null;
    if (taxPath != null) {
      while (taxPath.getChild() != null) {
        HierarchyResultSetEntry e = new HierarchyResultSetEntry();
        e.setLabel(taxPath.getScientificname());
        e.setRank(taxPath.getRank());
        e.setUri(wormsTaxonURL + Integer.toString(taxPath.getAphiaID()));
        e.setExternalID(Integer.toString(taxPath.getAphiaID()));
        ArrayList<String> array = new ArrayList<String>();
        if (parent != null) {
          array.add(wormsTaxonURL + parent.getAphiaID());
        }
        e.setHierarchy(array);
        rs.addEntry(e);
        parent = taxPath;
        taxPath = taxPath.getChild();
      }
    } else {
      LOGGER.error(" no classification for this id  " + externalID);
    }
    return rs;
  }

  /**
   * a factory method to build the Json for a list of aphiarecords
   * 
   * @param result
   * @param ar
   */
  private void setEntry(GFBioResultSet<SynonymsResultSetEntry> rs, final AphiaRecord[] synonyms) {
    SynonymsResultSetEntry e = new SynonymsResultSetEntry();
    ArrayList<String> array = new ArrayList<String>();
    for (AphiaRecord syn : synonyms) {
      array.add(syn.getScientificname());
    }
    e.setSynonyms(array);
    rs.addEntry(e);
  }

  /**
   * a factory method to build the Json for normal aphia entity
   * 
   * @param result
   * @param ar
   */
  private void setEntry(GFBioResultSet<SearchResultSetEntry> rs, final AphiaRecord ar) {
    SearchResultSetEntry e = new SearchResultSetEntry();
    if (ar.getScientificname() != null) {
      e.setLabel(ar.getScientificname());
    }
    if (ar.getKingdom() != null) {
      e.setKingdom(ar.getKingdom());
    }
    if (ar.getRank() != null) {
      e.setRank(ar.getRank());
    }
    if (ar.getStatus() != null) {
      e.setStatus(ar.getStatus());
    }
    if (ar.getUrl() != null) {
      e.setUri(ar.getUrl());
    }
    e.setExternalID(Integer.toString(ar.getAphiaID()));
    e.setInternal("false");
    Vernacular[] vern = wormsWSDAO.getVernacularByID(ar.getAphiaID());
    ArrayList<String> verns = new ArrayList<String>();
    if (vern != null) {
      for (Vernacular v : vern) {
        verns.add(v.getVernacular());
      }
      e.setCommonNames(verns);
    }
    e.setSourceTerminology(getAcronym());
    rs.addEntry(e);
  }

  private void setEntry(GFBioResultSet<AllBroaderResultSetEntry> rs,
      final Classification classify) {
    AllBroaderResultSetEntry e = new AllBroaderResultSetEntry();
    e.setRank(classify.getRank());
    e.setLabel(classify.getScientificname());
    e.setUri(wormsTaxonURL + Integer.toString(classify.getAphiaID()));
    e.setExternalID(Integer.toString(classify.getAphiaID()));
    rs.addEntry(e);
  }

  /**
   * Implements the getTermInfosOriginal method of the WSInterface Gets the informations of a taxon
   * given its URI with the original attributes
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<TermOriginalResultSetEntry> getTermInfosOriginal(String term_uri) {
    String externalID = term_uri;
    if (term_uri.startsWith(wormsTaxonURL)) {
      externalID = externalID.substring(wormsTaxonURL.length());
    }
    if (term_uri.startsWith(wormsTaxonLSID)) {
      externalID = externalID.substring(wormsTaxonLSID.length());
    }
    int aphiaId = Integer.parseInt(externalID);
    GFBioResultSet<TermOriginalResultSetEntry> rs =
        new GFBioResultSet<TermOriginalResultSetEntry>("worms");
    AphiaRecord rec = this.wormsWSDAO.getAphiaRecordByID(aphiaId);
    AphiaRecord[] children = this.wormsWSDAO.getAphiaChildrenByID(aphiaId);
    AphiaRecord[] synonyms = this.wormsWSDAO.getAphiaSynonymsByID(aphiaId);
    Vernacular[] vernaculars = this.wormsWSDAO.getVernacularByID(aphiaId);
    Classification classify = null;
    try {
      classify = this.wormsWSDAO.getAphiaClassificationByID(aphiaId);
    } catch (RemoteException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    Source[] sources = null;
    try {
      sources = this.wormsWSDAO.getSourcesByAphiaID(aphiaId);
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    TermOriginalResultSetEntry e = new TermOriginalResultSetEntry();
    if (rec != null) {
      String combinedName = combineName(rec.getScientificname(), rec.getAuthority());
      String acceptedName = combineName(rec.getValid_name(), rec.getValid_authority());

      if (!combinedName.equals(" ")) {
        e.setOriginalTermInfo("CombinedName", combinedName);
      }
      e.setOriginalTermInfo("aphiaID", Integer.toString(rec.getAphiaID()));
      if (rec.getStatus() != null) {
        e.setOriginalTermInfo("status", rec.getStatus());
      }
      if (rec.getRank() != null) {
        e.setOriginalTermInfo("rank", rec.getRank());
      }
      if (rec.getKingdom() != null) {
        e.setOriginalTermInfo("kingdom", rec.getKingdom());
      }
      if (synonyms != null) {
        ArrayList<String> synonymList = new ArrayList<String>();
        for (AphiaRecord synonym : synonyms) {
          String synonymName = combineName(synonym.getScientificname(), synonym.getAuthority());
          synonymList.add(synonymName);
        }
        e.setOriginalTermInfo("SynonymisedNames", synonymList);
      }
      ArrayList<String> verns = new ArrayList<String>();
      if (vernaculars != null) {
        for (Vernacular v : vernaculars) {
          verns.add(v.getVernacular());
        }
        e.setOriginalTermInfo("Vernaculars", verns);
      }
      if (rec.getStatus().equals("unaccepted") && rec.getUnacceptreason() != null) {
        e.setOriginalTermInfo("UnacceptReason", rec.getUnacceptreason());
      }
      if (!acceptedName.equals(combinedName)) {
        e.setOriginalTermInfo("AcceptedName", acceptedName);
      }
      ArrayList<String> parents = new ArrayList<String>();
      while (classify != null && classify.getChild() != null) {
        if (classify.getChild().getAphiaID() == aphiaId) {
          AphiaRecord parent = this.wormsWSDAO.getAphiaRecordByID(classify.getAphiaID());
          String parentName = combineName(parent.getScientificname(), parent.getAuthority());
          parents.add(parentName);
          break;
        }
        classify = classify.getChild();
      }
      e.setOriginalTermInfo("Parent", parents);
      if (sources != null) {
        ArrayList<String> sourcesList = new ArrayList<String>();
        for (Source source : sources) {
          if (source.getReference() != null) {
            sourcesList.add(source.getReference());
          }
        }
        e.setOriginalTermInfo("Sources", sourcesList);
      }
      if (children != null) {
        ArrayList<String> childrenList = new ArrayList<String>();
        for (AphiaRecord child : children) {
          String childName = combineName(child.getScientificname(), child.getAuthority());
          childrenList.add(childName);
        }
        e.setOriginalTermInfo("DirectChildren", childrenList);
      }
      String environment = rec.getIsMarine() == 1 ? "marine"
          : (rec.getIsBrackish() == 1 ? "brackish"
              : (rec.getIsFreshwater() == 1 ? "freshwater"
                  : (rec.getIsTerrestrial() == 1 ? "terrestrial" : "")));
      if (!environment.equals("")) {
        e.setOriginalTermInfo("Environment", environment);
      }
      if (rec.getLsid() != null) {
        e.setOriginalTermInfo("LSID", rec.getLsid());
      }
      if (rec.getModified() != null) {
        e.setOriginalTermInfo("Modified", rec.getModified());
      }
      if (rec.getCitation() != null) {
        e.setOriginalTermInfo("Citation", rec.getCitation());
      }
    }
    rs.addEntry(e);
    return rs;
  }

  /**
   * Implements the getTermInfosProcessed method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SearchResultSetEntry> getTermInfosProcessed(String term_uri) {
    String externalID = term_uri;
    if (term_uri.startsWith(wormsTaxonURL)) {
      externalID = externalID.substring(wormsTaxonURL.length());
    }
    if (term_uri.startsWith(wormsTaxonLSID)) {
      externalID = externalID.substring(wormsTaxonLSID.length());
    }
    int aphiaId = Integer.parseInt(externalID);
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("worms");
    AphiaRecord ar = this.wormsWSDAO.getAphiaRecordByID(aphiaId);
    setEntry(rs, ar);
    return rs;
  }

  /**
   * Implements the getTermInfosCombined method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes when mapped and original attributes when not
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<TermCombinedResultSetEntry> getTermInfosCombined(String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(wormsTaxonURL)) {
      externalID = externalID.substring(wormsTaxonURL.length());
    }
    if (uriOrexternalID.startsWith(wormsTaxonLSID)) {
      externalID = externalID.substring(wormsTaxonLSID.length());
    }

    int aphiaId = Integer.parseInt(externalID);
    GFBioResultSet<TermCombinedResultSetEntry> rs =
        new GFBioResultSet<TermCombinedResultSetEntry>("worms");
    AphiaRecord rec = this.wormsWSDAO.getAphiaRecordByID(aphiaId);
    AphiaRecord[] children = this.wormsWSDAO.getAphiaChildrenByID(aphiaId);
    AphiaRecord[] synonyms = this.wormsWSDAO.getAphiaSynonymsByID(aphiaId);
    Vernacular[] vernaculars = this.wormsWSDAO.getVernacularByID(aphiaId);
    Classification classify = null;
    try {
      classify = this.wormsWSDAO.getAphiaClassificationByID(aphiaId);
    } catch (RemoteException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    Source[] sources = null;
    try {
      sources = this.wormsWSDAO.getSourcesByAphiaID(aphiaId);
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    TermCombinedResultSetEntry e = new TermCombinedResultSetEntry();
    if (rec != null) {
      String combinedName = combineName(rec.getScientificname(), rec.getAuthority());
      String acceptedName = combineName(rec.getValid_name(), rec.getValid_authority());

      if (!combinedName.equals(" ")) {
        e.setLabel(combinedName);
      }
      e.setExternalID(Integer.toString(rec.getAphiaID()));
      if (rec.getStatus() != null) {
        e.setStatus(rec.getStatus());
      }
      if (rec.getRank() != null) {
        e.setRank(rec.getRank());
      }
      if (rec.getKingdom() != null) {
        e.setKingdom(rec.getKingdom());
      }
      e.setSourceTerminology(getAcronym());
      if (synonyms != null) {
        ArrayList<String> synonymList = new ArrayList<String>();
        for (AphiaRecord synonym : synonyms) {
          String synonymName = combineName(synonym.getScientificname(), synonym.getAuthority());
          synonymList.add(synonymName);
        }
        e.setSynonyms(synonymList);
      }
      ArrayList<String> verns = new ArrayList<String>();
      if (vernaculars != null) {
        for (Vernacular v : vernaculars) {
          verns.add(v.getVernacular());
        }
        e.setCommonNames(verns);
      }
      if (rec.getStatus().equals("unaccepted") && rec.getUnacceptreason() != null) {
        e.setOriginalTermInfo("UnacceptReason", rec.getUnacceptreason());
      }
      if (!acceptedName.equals(combinedName)) {
        e.setOriginalTermInfo("AcceptedName", acceptedName);
      }
      ArrayList<String> parents = new ArrayList<String>();
      while (classify != null) {
        if (classify.getChild().getAphiaID() == aphiaId) {
          AphiaRecord parent = this.wormsWSDAO.getAphiaRecordByID(classify.getAphiaID());
          String parentName = combineName(parent.getScientificname(), parent.getAuthority());
          parents.add(parentName);
          break;
        }
        classify = classify.getChild();
      }
      e.setOriginalTermInfo("Parent", parents);
      if (sources != null) {
        ArrayList<String> sourcesList = new ArrayList<String>();
        for (Source source : sources) {
          if (source.getReference() != null) {
            sourcesList.add(source.getReference());
          }
        }
        e.setOriginalTermInfo("Sources", sourcesList);
      }
      if (children != null) {
        ArrayList<String> childrenList = new ArrayList<String>();
        for (AphiaRecord child : children) {
          String childName = combineName(child.getScientificname(), child.getAuthority());
          childrenList.add(childName);
        }
        e.setOriginalTermInfo("DirectChildren", childrenList);
      }
      String environment = rec.getIsMarine() == 1 ? "marine"
          : (rec.getIsBrackish() == 1 ? "brackish"
              : (rec.getIsFreshwater() == 1 ? "freshwater"
                  : (rec.getIsTerrestrial() == 1 ? "terrestrial" : "")));
      if (!environment.equals("")) {
        e.setOriginalTermInfo("Environment", environment);
      }
      if (rec.getLsid() != null) {
        e.setOriginalTermInfo("LSID", rec.getLsid());
      }
      if (rec.getModified() != null) {
        e.setOriginalTermInfo("Modified", rec.getModified());
      }
      if (rec.getCitation() != null) {
        e.setOriginalTermInfo("Citation", rec.getCitation());
      }
    }
    rs.addEntry(e);
    return rs;
  }

  private String combineName(String name, String authority) {
    StringBuilder fullname = new StringBuilder();
    if (name != null) {
      fullname.append(name);
    }
    if (!name.equals("") && authority != null) {
      fullname.append(" " + authority);
    }
    return fullname.toString();
  }

  // FIXME move out, only a tool method
  public List<Integer> readIDList(final String dataFilePath) {
    List<Integer> result = new ArrayList<>();
    Path dataTitleFile = FileSystems.getDefault().getPath(dataFilePath);

    BufferedReader reader = null;

    try {
      reader = Files.newBufferedReader(dataTitleFile, StandardCharsets.UTF_8);
    } catch (IOException e) {

      e.printStackTrace();
    }
    String row = "";

    while (row != null) {
      try {
        row = reader.readLine();
      } catch (IOException e) {

        e.printStackTrace();
      }

      if (row != null) {
        row = row.trim();
        result.add(Integer.parseInt(row));
      }
    }
    return result;
  }


  public boolean isResponding() {
    AphiaNameServiceLocator locator = new AphiaNameServiceLocator();
    try {
      AphiaNameServicePortType port = locator.getAphiaNameServicePort();
      port.getAphiaRecords("Ixodes uriae", false, true, false, 0);
    } catch (ServiceException e) {
      LOGGER.error("WoRMS is not responding.");
      return false;
    } catch (RemoteException e) {
      LOGGER.error("WoRMS is not responding.");
      return false;
    }
    LOGGER.info("WoRMS is responding.");
    return true;
  }

  /**
   * Implements the getMetadata method of the WSInterface Gets the metadata information about the
   * webservice
   * 
   * @return GFBioResultSet
   */

  public GFBioResultSet<MetadataResultSetEntry> getMetadata() {
    GFBioResultSet<MetadataResultSetEntry> rs = new GFBioResultSet<MetadataResultSetEntry>("worms");
    MetadataResultSetEntry e = new MetadataResultSetEntry();
    e.setAcronym(getAcronym());
    e.setName(getName());
    e.setVersion(wormsConfig.getVersion());
    e.setDescription(getDescription());
    e.setKeywords(wormsConfig.getKeywords());
    e.setContact(wormsConfig.getContact());
    e.setUri(getURI());
    e.setStorage(wormsConfig.getStorage());
    e.setContribution(wormsConfig.getContribution());
    ArrayList<String> namespaces = new ArrayList<String>();
    namespaces.add(wormsTaxonLSID);
    e.setNamespaces(namespaces);
    ArrayList<String> domainUris = new ArrayList<String>();
    for (Domains d : wormsConfig.getWsDomains()) {
      domainUris.add(d.getUri());
    }
    e.setDomain(domainUris);
    rs.addEntry(e);
    return rs;
  }

  /**
   * Implements the getCapabilities method of the WSInterface Returns the available service
   * endpoints for the webservice
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<CapabilitiesResultSetEntry> getCapabilities() {
    GFBioResultSet<CapabilitiesResultSetEntry> rs =
        new GFBioResultSet<CapabilitiesResultSetEntry>("geonames");
    CapabilitiesResultSetEntry e = new CapabilitiesResultSetEntry();
    ArrayList<String> servicesArray = new ArrayList<String>();
    for (Services s : wormsConfig.getAvailableServices()) {
      servicesArray.add(s.toString());
    }
    e.setAvailableServices(servicesArray);

    ArrayList<String> modesArray = new ArrayList<String>();
    for (SearchModes m : wormsConfig.getSearchModes()) {
      modesArray.add(m.toString());
    }
    e.setSearchModes(modesArray);
    rs.addEntry(e);
    return rs;
  }


}
